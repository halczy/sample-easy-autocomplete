json.extract! car, :id, :vin, :manufacture, :created_at, :updated_at
json.url car_url(car, format: :json)
