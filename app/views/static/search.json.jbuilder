json.books do
  json.array!(@books) do |book|
    json.name book.title
    json.url book_path(book)
  end
end

json.cars do
  json.array!(@cars) do |car|
    json.name car.vin
    json.url car_path(car)
  end
end