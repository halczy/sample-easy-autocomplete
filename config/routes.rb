Rails.application.routes.draw do

  root 'static#index'

  get :search, controller: :static
  resources :books
  resources :cars

end
